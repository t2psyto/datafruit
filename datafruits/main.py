# -*- coding: utf-8 -*-
from flask import Flask, Response, request, render_template

import sqlite3, json
from datetime import datetime

#自作ユーティリティー関数
from util import isonow, keys_from_cursor, mapdict


app = Flask(__name__)

#インデックスページ：とりあえず hello を表示
@app.route('/')
def index():
    return render_template('hello.html', name="none")

@app.route('/hello')
@app.route('/hello/<username>')
def hello(username=None):
    return render_template('hello.html', name=username)


#グラフ表示ページ
@app.route('/view')
def view():
    return render_template('view.html')

#テーブル表示ページ
@app.route('/tableview')
def tableview():
    return render_template('tableview.html')


#値の入力用
@app.route('/input')
def input():
    temp = request.args.get("temp", None)
    timestamp = request.args.get("timestamp", isonow())
    name = request.args.get("name", "anonymous")
    print temp,timestamp,device
    conn = sqlite3.connect("db/atmosphere.db")
    cur = conn.cursor()
    cur.execute("insert into atmosphere(name,timestamp,temp) values(?,?,?)",
                            [name, timestamp, temp])
    conn.commit()
    conn.close()
    
    strmimetype = "application/json"

    data = {"name":name, "timestamp":timestamp, "temp":temp}
    dat = json.dumps(data)
    resp = Response(response=dat,
                    status=200,
                    mimetype=strmimetype)
    return(resp)
    

#jsonデータを返す用
@app.route('/json')
@app.route('/jsonp')
def jsondata():

    conn = sqlite3.connect("db/atmosphere.db")
    cur = conn.cursor()

    #ページ番号
    page = request.args.get("page", "1")

    if page == "all":
        cur.execute("select * from atmosphere order by timestamp;")
    else:
        
        linesofpage = 60  #1回のリクエストで返す行数

        #offset = Nページ目先頭となるデータの絶対行番号
        offset = linesofpage * (int(page) - 1)
        cur.execute("select * from atmosphere order by timestamp limit ? offset ?;", (linesofpage, offset))

    values = cur.fetchall()
    keys = keys_from_cursor(cur)

    data = mapdict(keys, values)

    dat = json.dumps(data)
    if request.path == "/json":
        strmimetype = "application/json"
    elif request.path == "/jsonp":
        
        strmimetype = "application/javascript"
        callback = request.args.get("callback", "callback")
        dat = u"%s(%s);" % (callback,dat,)
        print dat

    resp = Response(response=dat,
                    status=200,
                    mimetype=strmimetype)
    return(resp)


if __name__ == "__main__":
    app.run(debug=True)


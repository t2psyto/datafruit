from datetime import datetime
import random

DBFILE="db/atmosphere.db"

def mapdict(keys, values):
    return [dict(zip(keys, v)) for v in values]


def keys_from_cursor(cur):
    return [k[0] for k in cur.description]


def isonow():
    return datetime.isoformat(datetime.now().replace(microsecond=0)) + "+09:00"


def dummydata(conn):
    cur = conn.cursor()
    for m in range(59):
        # isoformat(jst = utc+9hour ) yyyy-mm-ddThh:mm:ss+09:00
        timestamp = datetime(2015, 8, 9, 13, m).isoformat() + "+09:00"
        cur.execute("insert into atmosphere(name,timestamp,temp) values(?,?,?)",
                            ["dev01", timestamp, "%2d" % (random.random() * 100,)])

    conn.commit()

def create_table(conn):
    cur = conn.cursor()
    cur.execute('drop table if exists atmosphere;')
    cur.execute('create table atmosphere(id integer primary key autoincrement, name text, timestamp text, temp real);')
    conn.commit()

def open_db():
    conn = sqlite3.connect(DBFILE)
    return conn

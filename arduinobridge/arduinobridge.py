import requests
from datetime import datetime

#PySerial
import serial
COMPORT="COM1"

dummy_from_arduino = "33.3"
device_name = "dev02"

serverurl = "http://localhost:5000/input"

def isonow():
    #returns: "2015-08-10T09:00+09:00"
    return datetime.isoformat(datetime.now().replace(microsecond=0)) + "+09:00"

def getdata_from_arduino(objcom):
    line = objcom.readline()   # read a '\n' terminated line
    return line

if __name__ == "__main__":
    ser = serial.Serial(COMPORT, 19200, timeout=1)

    while(True):
        str_timestamp = isonow()
        temp = getdata_from_arduino(ser)
        
        payload = {'temp': temp, 'name': device_name, "timestamp":str_timestamp}
        r = requests.get(serverurl, params=payload)
        
        print(peyload, r.text)
        time.sleep(10)

    ser.close()
